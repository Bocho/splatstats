﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SplatStats.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SplatStats.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Firefin,Ink Saver (Sub),Ink Recovery Up
        ///Forge,Special Duration Up,Ink Saver (Sub)
        ///Inkline,Defense Up,Damage Up
        ///Krak-On,Swim Speed Up,Defense Up
        ///Rockenberg,Run Speed Up,Swim Speed Up
        ///Skalop,Quick Respawn,Special Saver
        ///Splash Mob,Ink Saver (Main),Run Speed Up
        ///Squidforce,Damage Up,Ink Saver (Main)
        ///Takoroka,Special Charge Up,Special Duration Up
        ///Tentatek,Ink Recovery Up,Quick Super Jump
        ///Zekko,Special Saver,Special Charge Up
        ///Zink,Quick Super Jump,Quick Respawn
        ///amiibo,None,None
        ///Cuttlegear,None,None
        ///F [rest of string was truncated]&quot;;.
        /// </summary>
        public static string brands {
            get {
                return ResourceManager.GetString("brands", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aloha Shirt,Forge,Ink Recovery Up,1,700
        ///Anchor Sweat,SquidForce,Cold-Blooded,2,3000
        ///Armor Jacket Replica,Cuttlegear,Special Charge Up,3,0
        ///B-ball Jersey (Away),Zink,Ink Saver (Sub),1,800
        ///B-ball Jersey (Home),Zink,Special Saver,2,2300
        ///Baby-Jelly Shirt,Splash Mob,Defense Up,2,2200
        ///Baseball Jersey,Firefin,Special Charge Up,3,10000
        ///Basic Tee,SquidForce,Quick Respawn,1,0
        ///Berry Ski Jacket,Inkline,Special Duration Up,2,3000
        ///Black Anchor Tee,SquidForce,Recon,2,2800
        ///Black Baseball LS,Rockenberg,Swim Speed U [rest of string was truncated]&quot;;.
        /// </summary>
        public static string clothes {
            get {
                return ResourceManager.GetString("clothes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Backwards Cap,Zekko,Quick Respawn,1,700
        ///Armor Helmet Replica,Cuttlegear,Tenacity,3,0
        ///B-ball Headband,Zink,Opening Gambit,1,300
        ///Bamboo Hat,Inkline,Ink Saver (Main),2,2200
        ///Bike Helmet,Skalop,Ink Recovery Up,3,11000
        ///Black Arrowbands,Zekko,Tenacity,2,2800
        ///Blowfish Bell Hat,Firefin,Ink Recovery Up,1,700
        ///Bobble Hat,Splash Mob,Quick Super Jump,2,2000	
        ///Camo Mesh,Firefin,Swim Speed Up,2,2500	
        ///Camping Hat,Inkline,Special Duration Up,1,800	
        ///Cycle King Cap,Tentatek,Defense Up,2,2400	
        ///Clycling Cap,Zink,Bomb R [rest of string was truncated]&quot;;.
        /// </summary>
        public static string hats {
            get {
                return ResourceManager.GetString("hats", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Acerola Rain Boots,Inkline,Run Speed Up,1,600
        ///Armor Boot Replicas,Cuttlegear,Ink Saver (Main),3,0
        ///Banana Basics,Krak-On,Bomb Sniffer,1,400
        ///Black Seahorses,Zink,Swim Speed Up,2,2000
        ///Black Trainers,Tentatek,Quick Respawn,1,500
        ///Blue Lo-Tops,Zekko,Defense Up,1,800
        ///Blue Moto Boots,Rockenberg,Ink Resistance Up,3,12000
        ///Blue Slip-Ons,Krak-On,Bomb Range Up,1,300
        ///Blueberry Casuals,Krak-On,Ink Saver (Sub),1,700
        ///Cherry Kicks,Rockenberg,Stealth Jump,2,2800
        ///Choco Clogs,Krak-On,Quick Respawn,2,1800
        ///Clownfish Ba [rest of string was truncated]&quot;;.
        /// </summary>
        public static string shoes {
            get {
                return ResourceManager.GetString("shoes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Damage Up,no,% Extra Damage,0.0,2.9,5.7,8.3,9.2,10.8,11.6,13.1,13.9,15.3,16.0,16.7,17.3,18.0,18.6,19.2,19.8,20.4,20.9,21.5,22.0,22.5,23.0,23.5,23.9,24.4,24.8,25.2,25.6,26.0,26.3,27.0,27.3,27.9,28.1,28.6,28.8,29.3,29.7,29.9
        ///Defense Up,no,% Total Damage Taken,100.0,98.4,96.9,95.6,95.2,94.3,93.9,93.2,92.8,92.2,91.8,91.5,91.2,90.9,90.6,90.4,90.1,89.8,89.6,89.3,89.1,88.9,88.7,88.5,88.3,88.1,87.9,87.7,87.6,87.4,87.2,87.0,86.8,86.6,86.5,86.3,86.2,86.0,85.8,85.7
        ///Run Speed Up,no,DU/frame running speed,0.960,1.007, [rest of string was truncated]&quot;;.
        /// </summary>
        public static string stats {
            get {
                return ResourceManager.GetString("stats", resourceCulture);
            }
        }
    }
}
