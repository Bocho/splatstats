﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Controls;

namespace SplatStats
{
    class Clothing
    {
        public Brand brand { get; set; }
        public string name { get; set; }
        public int cost { get; set; }
        public int starCount { get; set; }
        public Stat mainAbility { get; set; }

        public override string ToString()
        {
            return name +" (" + mainAbility.type + ")";
        }

    }

    class Brand
    {
        public string name { get; set; }
        public Stat moreLikely { get; set; }
        public Stat lessLikely { get; set; }
    }

    class Weapon
    {
        public string name { get; set; }
        public float damage { get; set; }
        public int shotsToKill { get; set; }

        public void setSTK()
        {
            int i = 1;
            while (damage * i < 100f)
            {
                i++;
            }
            shotsToKill = i;
        }
    }

    class Stat
    {
        public string type { get; set; }
        public bool savesFrames { get; set; }
        public string Units { get; set; }
        public float[] upgrades { get; set; }
        public int[] upgradesF { get; set; }
        public bool standaloneAbility { get; set; }

        public float[] minmax { get; set; }
        public int[] minmaxF { get; set; }

        public float currentBonus { get; set; }

        public override string ToString()
        {
            return type;
        }
    }

    class EquippedAbility
    {
        public Stat type { get; set; }
        public int Mains { get; set; }
        public int Subs { get; set; }

        public float value { get; set; }
        public int valueF { get; set; }

        private float calculateEffect()
        {
            return 0;
        }

        public string ToName()
        {
            return type.type;
        }

        public string ToDesc()
        {
            if (type.standaloneAbility)
            {
                return ": " + type.Units;
            }

            float val;

            if (!type.savesFrames && !type.standaloneAbility) {
                if (type.type == "Ink Saver (Sub)")
                {
                    float f = 100 - value;
                    f = (float)Math.Round(f, 2);
                    return ": " + f.ToString() + type.Units;
                }

                else if (type.type == "Swim Speed Up" || type.type == "Run Speed Up" || type.type == "Bomb Range Up")
                {
                    string s = "";
                    if (type.type == "Swim Speed Up")
                        s = "Swim Speed";
                    else if (type.type == "Run Speed Up")
                        s = "Run Speed";
                    else
                        s = "Sub Throw Dist.";
                    
                    if (type.upgrades != null)
                    {
                        val = value - type.upgrades[0];
                        val = (val / type.upgrades[0]) * 100;
                        return ": " + val.ToString("0.00") + "% increased " + s + " (" + (value * 60).ToString("0.00") + "/" + (type.upgrades.Last() * 60).ToString("0.00") + "DU per second / max)";
                    }
                    else
                    {
                        val = value - type.minmax[0];
                        val = (val / type.minmax[0]) * 100;
                        return ": " + val.ToString("0.00") + "% increased " + s + " (" + (value * 60).ToString("0.00") + "/" + (type.minmax[1] * 60).ToString("0.00") + "DU per second / max)";
                    }
                    
                }

                if (type.type == "Damage Up")
                {
                    return ": " + (value * 100).ToString("0.00") + type.Units;
                }

                if (type.upgrades != null)
                {
                    val = value - type.upgrades[0];
                    val = (val / type.upgrades[0]) * 100;
                }
                else
                {
                    val = value - type.minmax[0];
                    val = (val / type.minmax[0]) * 100;

                    if (val < 0)
                        val = val * -1;
                }

                if (type.type == "Defense Up")
                {
                    val = 100 - val;
                }
                return ": " + val.ToString("0.00") + type.Units;
            }
            if (type.savesFrames && !type.standaloneAbility) 
            {
                float seconds = valueF / 60f;
                return ": " + valueF.ToString() + type.Units + " (Out of " + type.upgradesF.Last().ToString() +  ") (" + seconds.ToString("0.00") +" second increase)";
            }

            return type.type;
        }

        public override string ToString()
        {
            if (type.standaloneAbility)
            {
                return type.type + " (" + type.Units + ")";
            }

            if (!type.savesFrames && !type.standaloneAbility)
                return type.type + " (" + value.ToString() + type.Units + ")";
            if (type.savesFrames && !type.standaloneAbility)
                return type.type + " (" + valueF.ToString() + type.Units + ")";

            return type.type;
        }
    }
    
}
