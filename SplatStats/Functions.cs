﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Controls;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace SplatStats
{
    class Functions
    {

        static public float Lerp(float valA, float valB, float f)
        {
            return (valA * (1.0f - f)) + (valB * f);
        }

        static public float CalcPercentage(int mains, int subs)
        {
            int score = (mains * 10) + (subs * 3);
            double f = 2 * (score / 60f) - Math.Pow((score / 60f),2f);

            return (float)f;
        }

        static public int getArrayLoc(int main, int sub)
        {
            if (main == 0 && sub == 1)
                return 1;
            else if (main == 0 && sub == 2)
                return 2;
            else if (main == 0 && sub == 3)
                return 3;
            else if (main == 1 && sub == 0)
                return 4;
            else if (main == 0 && sub == 4)
                return 5;
            else if (main == 1 && sub == 1)
                return 6;
            else if (main == 0 && sub == 5)
                return 7;
            else if (main == 1 && sub == 2)
                return 8;

            else if (main == 0 && sub == 6)
                return 9;
            else if (main == 1 && sub == 3)
                return 10;
            else if (main == 2 && sub == 0)
                return 11;
            else if (main == 0 && sub == 7)
                return 12;
            else if (main == 1 && sub == 4)
                return 13;

            else if (main == 2 && sub == 1)
                return 14;
            else if (main == 0 && sub == 8)
                return 15;
            else if (main == 1 && sub == 5)
                return 16;
            else if (main == 2 && sub == 2)
                return 17;
            else if (main == 0 && sub == 9)
                return 18;

            else if (main == 1 && sub == 6)
                return 19;
            else if (main == 2 && sub == 3)
                return 20;
            else if (main == 3 && sub == 0)
                return 21;
            else if (main == 1 && sub == 7)
                return 22;
            else if (main == 2 && sub == 4)
                return 23;

            else if (main == 3 && sub == 1)
                return 24;
            else if (main == 1 && sub == 8)
                return 25;
            else if (main == 2 && sub == 5)
                return 26;
            else if (main == 3 && sub == 2)
                return 27;
            else if (main == 1 && sub == 9)
                return 28;

            if (main == 2 && sub == 6)
                return 29;
            if (main == 3 && sub == 3)
                return 30;
            if (main == 2 && sub == 7)
                return 31;
            if (main == 3 && sub == 4)
                return 32;
            if (main == 2 && sub == 8)
                return 33;

            if (main == 3 && sub == 5)
                return 34;
            if (main == 2 && sub == 9)
                return 35;
            if (main == 3 && sub == 6)
                return 36;
            if (main == 3 && sub == 7)
                return 37;
            if (main == 3 && sub == 8)
                return 38;
            if (main == 3 && sub == 9)
                return 39;

            return 0;
        }

        static public void setText(TextBlock t, ComboBox C)
        {

            
            Clothing c = (Clothing)C.SelectedItem;
            t.Text = "";
            t.Inlines.Add(new Bold(new Run("Brand: ")));
            t.Inlines.Add(c.brand.name + Environment.NewLine);
            t.Inlines.Add(new Bold(new Run("Main: ")));
            t.Inlines.Add(c.mainAbility.type + Environment.NewLine);
            t.Inlines.Add(new Bold(new Run("Favoured: ")));
            t.Inlines.Add(c.brand.moreLikely.type);
            t.Inlines.Add(new Bold(new Run(" Unfavoured: ")));
            t.Inlines.Add(c.brand.lessLikely.type);
        }

        static public void readInput(List<Stat> Stats, List<Brand> Brands, List<Clothing> Clothes, List<Clothing> Hats, List<Clothing> Shoes, List<EquippedAbility> Abilities)
        {
            string[] brandstring;
            string[] statstring;
            string[] clothstring;
            string[] hatstring;
            string[] shoestring;

            if (File.Exists(Environment.CurrentDirectory + @"\brands.dat"))
                brandstring = File.ReadAllLines(@"brands.dat");
            else
                brandstring = Properties.Resources.brands.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            if (File.Exists(Environment.CurrentDirectory + @"\stats.dat"))
                statstring = File.ReadAllLines(@"stats.dat");
            else
                statstring = Properties.Resources.stats.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            if (File.Exists(Environment.CurrentDirectory + @"\clothes.dat"))
                clothstring = File.ReadAllLines(@"clothes.dat");
            else
                clothstring = Properties.Resources.clothes.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            if (File.Exists(Environment.CurrentDirectory + @"\hats.dat"))
                hatstring = File.ReadAllLines(@"hats.dat");
            else
                hatstring = Properties.Resources.hats.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            if (File.Exists(Environment.CurrentDirectory + @"\shoes.dat"))
                shoestring = File.ReadAllLines(@"shoes.dat");
            else
                shoestring = Properties.Resources.shoes.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            foreach (string s in statstring)
            {
                string[] s2 = s.Split(',');

                Stat stat = new Stat();
                stat.type = s2[0];
                if (s2[1].Contains("yes"))
                    stat.savesFrames = true;
                else stat.savesFrames = false;

                stat.Units = s2[2];
                
                if (s2.Length > 5) 
                { 
                    if (!stat.savesFrames)
                    {
                        float[] values = new float[40];

                        for (int i = 3; i < s2.Length; i++)
                        {
                            values[i - 3] = float.Parse(s2[i]);
                        }
                        stat.upgrades = values;
                    }
                    else if (stat.savesFrames)
                    {
                        int[] values = new int[40];

                        for (int i = 3; i < s2.Length; i++)
                        {
                            values[i - 3] = int.Parse(s2[i]);
                        }
                        stat.upgradesF = values;
                    }
                    stat.standaloneAbility = false;
                }
                else if (s2.Length <= 3) 
                {
                    stat.standaloneAbility = true;
                }
                else
                {
                    float[] t = new float[] {float.Parse(s2[3]), float.Parse(s2[4])};
                    stat.minmax = t;
                }

                Stats.Add(stat);
            }

            foreach (Stat s in Stats)
            {
                EquippedAbility a = new EquippedAbility();
                a.type = s;

                Abilities.Add(a);
            }

            foreach (string b in brandstring)
            {

                string[] s2 = b.Split(',');

                Brand br = new Brand();
                br.name = s2[0];

                foreach (Stat st in Stats)
                {
                    if (st.type == s2[1])
                    {
                        br.moreLikely = st;
                    }
                    if (st.type == s2[2])
                    {
                        br.lessLikely = st;
                    }
                }

                if (s2[1] == "None")
                {
                    Stat s = new Stat();
                    s.type = "NONE";
                    br.moreLikely = s;
                }

                if (s2[2] == "None")
                {
                    Stat s = new Stat();
                    s.type = "NONE";
                    br.lessLikely = s;
                }

                Brands.Add(br);
            }

            foreach (string s in clothstring)
            {
                string[] s2 = s.Split(',');
                Clothing c = new Clothing();
                c.name = s2[0];
                c.cost = int.Parse(s2[4]);
                c.starCount = int.Parse(s2[3]);
                foreach (Brand br in Brands)
                {
                    if (br.name == s2[1])
                    {
                        c.brand = br;
                    }
                }
                foreach (Stat st in Stats)
                {
                    if (st.type == s2[2])
                    {
                        c.mainAbility = st;
                    }
                }

                Clothes.Add(c);
            }

            foreach (string s in hatstring)
            {
                string[] s2 = s.Split(',');
                Clothing c = new Clothing();
                c.name = s2[0];
                c.cost = int.Parse(s2[4]);
                c.starCount = int.Parse(s2[3]);
                foreach (Brand br in Brands)
                {
                    if (br.name == s2[1])
                    {
                        c.brand = br;
                    }
                }
                foreach (Stat st in Stats)
                {
                    if (st.type == s2[2])
                    {
                        c.mainAbility = st;
                    }
                }

                Hats.Add(c);
            }

            foreach (string s in shoestring)
            {
                string[] s2 = s.Split(',');
                Clothing c = new Clothing();
                c.name = s2[0];
                c.cost = int.Parse(s2[4]);
                c.starCount = int.Parse(s2[3]);
                foreach (Brand br in Brands)
                {
                    if (br.name == s2[1])
                    {
                        c.brand = br;
                    }
                }
                foreach (Stat st in Stats)
                {
                    if (st.type == s2[2])
                    {
                        c.mainAbility = st;
                    }
                }

                Shoes.Add(c);
            }

        }
    }
}
