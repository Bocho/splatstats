﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SplatStats
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Stat> Stats = new List<Stat>();
        List<Brand> Brands = new List<Brand>();
        List<Clothing> Clothes = new List<Clothing>();
        List<Clothing> Hats = new List<Clothing>();
        List<Clothing> Shoes = new List<Clothing>();
        List<Weapon> Weapons = new List<Weapon>();

        List<EquippedAbility> Abilities = new List<EquippedAbility>();
       

        TextBlock[] AbilityLabels = new TextBlock[12];

        Clothing[] Recalculation_Clothing = new Clothing[3];
        Stat[] Recalculation_Stats = new Stat[9];

        int totalAbilitiesInstalled;

        public MainWindow()
        {
            InitializeComponent();
            Functions.readInput(Stats, Brands, Clothes, Hats, Shoes, Abilities);

            AbilityLabels[0] = AB1;
            AbilityLabels[1] = AB2;
            AbilityLabels[2] = AB3;
            AbilityLabels[3] = AB4;
            AbilityLabels[4] = AB5;
            AbilityLabels[5] = AB6;
            AbilityLabels[6] = AB7;
            AbilityLabels[7] = AB8;
            AbilityLabels[8] = AB9;
            AbilityLabels[9] = AB10;
            AbilityLabels[10] = AB11;
            AbilityLabels[11] = AB12;

            var s = from p in Clothes
                    orderby p.name ascending
                    select p;
                
            ClothesObj.ItemsSource = s.ToList();
            ClothesObj.SelectedIndex = 0;

            s = from p in Hats
                    orderby p.name ascending
                    select p;
            
            HatObj.ItemsSource = s.ToList();
            HatObj.SelectedIndex = 0;

            s = from p in Shoes
                    orderby p.name ascending
                    select p;
           
            ShoesObj.ItemsSource = s.ToList();
            ShoesObj.SelectedIndex = 0;

            var f = from st in Stats
                    where st.upgrades != null || st.upgradesF != null || st.minmax != null && !st.standaloneAbility
                    select st;

            List<Stat> sta = new List<Stat>();
            Stat c = new Stat();
            c.type = "NONE";
            sta.Add(c);
            sta.AddRange(f.ToList());

            HatSub1.ItemsSource = sta;
            HatSub2.ItemsSource = sta;
            HatSub3.ItemsSource = sta;
            ClothSub1.ItemsSource = sta;
            ClothSub2.ItemsSource = sta;
            ClothSub3.ItemsSource = sta;
            ShoeSub1.ItemsSource = sta;
            ShoeSub2.ItemsSource = sta;
            ShoeSub3.ItemsSource = sta;
        }

        private void Hat_SelChanged(object sender, SelectionChangedEventArgs e)
        {
            Functions.setText(HatMAbility, HatObj);
        }

        private void Shoes_SelChanged(object sender, SelectionChangedEventArgs e)
        {
            Functions.setText(ShoesMAbility, ShoesObj);
        }

        private void Clothes_SelChanged(object sender, SelectionChangedEventArgs e)
        {
            Functions.setText(ClothMAbility, ClothesObj);
        }

        private void Recalc(object sender, RoutedEventArgs e)
        {
            totalAbilitiesInstalled = 0;

            foreach (TextBlock lbl in AbilityLabels)
            {
                lbl.Text = "";
            }

            Recalculation_Clothing[0] = (Clothing)HatObj.SelectedItem;
            Recalculation_Clothing[1] = (Clothing)ClothesObj.SelectedItem;
            Recalculation_Clothing[2] = (Clothing)ShoesObj.SelectedItem;

            Recalculation_Stats[0] = (Stat)HatSub1.SelectedItem;
            Recalculation_Stats[1] = (Stat)HatSub2.SelectedItem;
            Recalculation_Stats[2] = (Stat)HatSub3.SelectedItem;

            Recalculation_Stats[3] = (Stat)ClothSub1.SelectedItem;
            Recalculation_Stats[4] = (Stat)ClothSub2.SelectedItem;
            Recalculation_Stats[5] = (Stat)ClothSub3.SelectedItem;

            Recalculation_Stats[6] = (Stat)ShoeSub1.SelectedItem;
            Recalculation_Stats[7] = (Stat)ShoeSub2.SelectedItem;
            Recalculation_Stats[8] = (Stat)ShoeSub3.SelectedItem;

            foreach (EquippedAbility a in Abilities)
            {
                a.Mains = 0;
                a.Subs = 0;
                a.value = 0;
                a.valueF = 0;

                foreach (Clothing c in Recalculation_Clothing)
                {
                    if (c.mainAbility == a.type)
                        a.Mains++;
                }

                foreach (Stat s in Recalculation_Stats)
                {
                    if (s == a.type)
                        a.Subs++;
                }

                if (!a.type.savesFrames && !a.type.standaloneAbility)
                {
                    if (a.type.minmax == null)
                        a.value = a.type.upgrades[Functions.getArrayLoc(a.Mains, a.Subs)];
                    else
                        a.value = Functions.Lerp(a.type.minmax[0], a.type.minmax[1], Functions.CalcPercentage(a.Mains, a.Subs));
                }
                else if (a.type.savesFrames)
                    a.valueF = a.type.upgradesF[Functions.getArrayLoc(a.Mains, a.Subs)];

                if (a.Mains != 0 || a.Subs != 0)
                {
                    AbilityLabels[totalAbilitiesInstalled].Text = "";
                    AbilityLabels[totalAbilitiesInstalled].Inlines.Add(new Bold(new Run(a.ToName())));
                    AbilityLabels[totalAbilitiesInstalled].Inlines.Add(a.ToDesc());
                    totalAbilitiesInstalled++;
                }
            }


        }

        
    }
}
