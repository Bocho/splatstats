# SplatStats V0.9#

![image.jpg](https://bitbucket.org/repo/zbAn44/images/3046993019-image.jpg)

**SplatStats** is a C# application for determining stat gains from Main and Sub Abilities.

### Installation Instructions ###

* Download splatstats.exe from the Release Folder
* Launch Application

### TODO ###

* Have Stats derive percentage from 2*(Ability/60) - (Ability/60)^2 -- IMPLEMENTED (For all stats except those framebased ones(Quick Respawn, Quick Super Jump, Ink Recovery Up))
* Add Weapon Information and Stats to the App
* Have the Damage Ability calculate how much damage each shot would give with a selected Weapon/Defense Stat.

### Extensibility ###

SplatStats was designed for extensibility in mind. In the Source Folder there is a folder called Updates. The Updates folder contains a list of .dat files, which are a copy of the files SplatStats uses internally. In the event SplatStats is no longer maintained, it is possible to edit these .dat files (They're plaintext) with updated information, provided you follow the syntax in the .dat file.

For brands.dat, the syntax is name,favoured sub,unfavoured sub

For clothes.dat, hats.dat and shoes.dat, the syntax is name,brand,mainStat,StarLevel,Cost

For stats.dat, the syntax differs based on whether the ability is available as a Sub or not.

For Stats available as a Sub, the syntax is name,isNonDecimal,AbilityString(What you want to see when listed in the Abilities List),stats(40 values go here). Those 40 values use the following ordering: 0Main/0Sub,0Main/1Sub,0Main/2Sub,0Main/3Sub,1Main/0Sub,0Main/4Sub,1Main/1Sub,0Main/5Sub,1Main/2Sub,0Main/6Sub,1Main/3Sub,2Main/0Sub,0Main/7Sub,1Main/4Sub,2Main/1Sub,0Main/8Sub,1Main/5Sub,2Main/2Sub,0Main/9Sub,1Main/6Sub,2Main/3Sub,3Main/0Sub,1Main/7Sub,2Main/4Sub,3Main/1Sub,1Main/8Sub,2Main/5Sub,3Main/2Sub,1Main/9Sub,2Main/6Sub,3Main/3Sub,2Main/7Sub,3Main/4Sub,2Main/8Sub,3Main/5Sub,2Main/9Sub,3Main/6Sub,3Main/7Sub,3Main/8Sub,3Main/9Sub.

Stats available only as a Main are much more simple. Their Syntax is name,no,AbilityString(What you want to see when listed in the Abilities List).


In all likeliness, Stats won't ever have to be changed. Clothes may be added, however.